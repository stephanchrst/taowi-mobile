package org.evenos.webservice.fields;

import java.util.ArrayList;
import java.util.List;

import com.alexgilleran.icesoap.annotation.XMLField;
import com.alexgilleran.icesoap.annotation.XMLObject;

@XMLObject("//ParamValues")
public class ParamValues {
	@XMLField("field")
	private List<DataField> dataFields;

	public ParamValues() {
		dataFields = new ArrayList<DataField>();
	}

	public void addDataField(String columnName, String columnValue) {
		DataField field = new DataField();
		field.setColumnName(columnName);
		field.setValue(columnValue);
		dataFields.add(field);
	}

	public List<DataField> getDataFields() {
		return dataFields;
	}
}
