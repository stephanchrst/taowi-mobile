package org.evenos.webservice.fields;

import com.alexgilleran.icesoap.annotation.XMLField;
import com.alexgilleran.icesoap.annotation.XMLObject;

@XMLObject("//RunProcessResponse")
public class RunProcessResponse {

	@XMLField("Error")
	private String error;
	@XMLField("Summary")
	private String summary;
	@XMLField("LogInfo")
	private String logInfo;

	@XMLField("@IsError")
	private String isError;

	public String getError() {
		return error;
	}

	public String getSummary() {
		return summary;
	}

	public String getLogInfo() {
		return logInfo;
	}

	public String getIsError() {
		return isError;
	}

	@Override
	public String toString() {
		return "RunProcessResponse - IsError: " + isError + " - Error: " + error + " - Summary: " + summary + " - LogInfo: " + logInfo;
	}

}
