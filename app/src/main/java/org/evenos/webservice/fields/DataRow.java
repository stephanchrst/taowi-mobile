package org.evenos.webservice.fields;

import java.util.ArrayList;
import java.util.List;

import com.alexgilleran.icesoap.annotation.XMLField;
import com.alexgilleran.icesoap.annotation.XMLObject;

@XMLObject("//DataRow")
public class DataRow {

	@XMLField("field")
	private List<DataField> dataFields;

	public DataRow() {
		dataFields = new ArrayList<DataField>();
	}

	public String getValueOfField(String columnName) {
		for (DataField field : dataFields) {
			if (field.getColumnName().equalsIgnoreCase(columnName))
				return field.getValue();
		}

		return "";
	}

	public List<DataField> getDataFields() {
		return dataFields;
	}

	public void addDataField(String columnName, String columnValue) {
		DataField field = new DataField();
		field.setColumnName(columnName);
		field.setValue(columnValue);
		dataFields.add(field);
	}

	public String toString() {
		String retVal = "";
		for (DataField field : dataFields) {
			retVal = retVal + field.toString() + ", ";
		}
		if (retVal.length() > 0)
			retVal = retVal.substring(0, retVal.length() - 2);
		return retVal;
	}

}
