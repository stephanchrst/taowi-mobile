package org.evenos.webservice.fields;

import com.alexgilleran.icesoap.annotation.XMLField;
import com.alexgilleran.icesoap.annotation.XMLObject;

@XMLObject("//field")
public class DataField {

	@XMLField("val")
	private String value;

	@XMLField("@column")
	private String columnName;

	public DataField() {

	}

	public DataField(String columnName, String value) {
		this.columnName = columnName;
		this.value = value;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getColumnName() {
		return columnName;
	}

	public String getValue() {
		return value;
	}

	public String toString() {
		return columnName + " = " + value;
	}

}
