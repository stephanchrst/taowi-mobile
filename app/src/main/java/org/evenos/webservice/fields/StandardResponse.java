package org.evenos.webservice.fields;

import com.alexgilleran.icesoap.annotation.XMLField;
import com.alexgilleran.icesoap.annotation.XMLObject;

@XMLObject("//StandardResponse")
public class StandardResponse {

	@XMLField("Error")
	private String error;

	@XMLField("@IsError")
	private String isError;
	
	@XMLField("@RecordID")
	private String recordID;

	public String getError() {
		return error;
	}

	public String getIsError() {
		return isError;
	}

	public String getRecordID() {
		return recordID;
	}

}
