package org.evenos.webservice.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.evenos.webservice.enums.WebServiceOperationName;
import org.evenos.webservice.fields.DataRow;
import org.evenos.webservice.fields.RunProcessResponse;
import org.evenos.webservice.fields.StandardResponse;
import org.evenos.webservice.nodes.ADLoginRequest;
import org.evenos.webservice.nodes.ModelCRUD;
import org.evenos.webservice.nodes.ModelGetList;
import org.evenos.webservice.nodes.ModelRunProcess;
import org.evenos.webservice.nodes.ModelSetDocAction;

import com.alexgilleran.icesoap.observer.SOAP11ListObserver;
import com.alexgilleran.icesoap.observer.SOAP11Observer;
import com.alexgilleran.icesoap.request.RequestFactory;
import com.alexgilleran.icesoap.request.SOAP11ListRequest;
import com.alexgilleran.icesoap.request.SOAP11Request;
import com.alexgilleran.icesoap.request.impl.RequestFactoryImpl;

public class WebServiceClient {

	private static final String SOAP_ACTION = "";

	private RequestFactory requestFactory;
	private ADLoginRequest loginRequest;
	private String endpointAddress;

	private List<SOAP11ListRequest<?>> listRequests = new ArrayList<SOAP11ListRequest<?>>();
	private List<SOAP11Request<?>> requests = new ArrayList<SOAP11Request<?>>();
	private List<SOAP11ListObserver<?>> listObservers = new ArrayList<SOAP11ListObserver<?>>();
	private List<SOAP11Observer<?>> observers = new ArrayList<SOAP11Observer<?>>();

	public WebServiceClient() {
		if (requestFactory == null)
			requestFactory = new RequestFactoryImpl();
	}

	public WebServiceClient(String endpoint) {
		this();
		this.endpointAddress = endpoint;
	}

	public void setADLoginRequest(ADLoginRequest loginRequest) {
		this.loginRequest = loginRequest;
	}

	public void setEndpointAddress(String endpointAddress) {
		this.endpointAddress = endpointAddress;
	}

	public void executeQuery(ModelCRUD modelCRUD, SOAP11ListObserver<DataRow> soapObserver) {
		WebServiceOperation queryData = new WebServiceOperation(WebServiceOperationName.queryData, modelCRUD, loginRequest);

		SOAP11ListRequest<DataRow> dataFieldRequest = requestFactory.buildListRequest(endpointAddress, queryData, SOAP_ACTION,
				DataRow.class);

		dataFieldRequest.execute(soapObserver);
		listRequests.add(dataFieldRequest);
		listObservers.add(soapObserver);
	}

	public void executeRead(ModelCRUD modelCRUD, SOAP11ListObserver<DataRow> soapObserver) {
		WebServiceOperation queryData = new WebServiceOperation(WebServiceOperationName.readData, modelCRUD, loginRequest);

		SOAP11ListRequest<DataRow> dataFieldRequest = requestFactory.buildListRequest(endpointAddress, queryData, SOAP_ACTION,
				DataRow.class);

		dataFieldRequest.execute(soapObserver);
		listRequests.add(dataFieldRequest);
		listObservers.add(soapObserver);
	}

	public void executeRunProcess(ModelRunProcess modelRunProcess, SOAP11Observer<RunProcessResponse> observer) {
		WebServiceOperation runProcess = new WebServiceOperation(WebServiceOperationName.runProcess, modelRunProcess, loginRequest);
		SOAP11Request<RunProcessResponse> request = requestFactory.buildRequest(endpointAddress, runProcess, SOAP_ACTION,
				RunProcessResponse.class);
		request.execute(observer);
		requests.add(request);
		observers.add(observer);
	}

	public void executeGetList(ModelGetList modelGetList, SOAP11ListObserver<DataRow> observer) {
		WebServiceOperation getList = new WebServiceOperation(WebServiceOperationName.getList, modelGetList, loginRequest);
		SOAP11ListRequest<DataRow> request = requestFactory.buildListRequest(endpointAddress, getList, SOAP_ACTION, DataRow.class);
		request.execute(observer);
		listRequests.add(request);
		listObservers.add(observer);
	}

	public void executeSetDocAction(ModelSetDocAction modelSetDocAction, SOAP11Observer<StandardResponse> observer) {
		WebServiceOperation setDocAction = new WebServiceOperation(WebServiceOperationName.setDocAction, modelSetDocAction, loginRequest);
		SOAP11Request<StandardResponse> request = requestFactory.buildRequest(endpointAddress, setDocAction, SOAP_ACTION,
				StandardResponse.class);
		request.execute(observer);
		requests.add(request);
		observers.add(observer);
	}

	public void executeCreateData(ModelCRUD modelCRUD, SOAP11Observer<StandardResponse> observer) {
		WebServiceOperation createData = new WebServiceOperation(WebServiceOperationName.createData, modelCRUD, loginRequest);
		SOAP11Request<StandardResponse> request = requestFactory.buildRequest(endpointAddress, createData, SOAP_ACTION,
				StandardResponse.class);
		request.execute(observer);
		requests.add(request);
		observers.add(observer);
	}

	public void executeCreateUpdateData(ModelCRUD modelCRUD, SOAP11Observer<StandardResponse> observer) {
		WebServiceOperation createUpdateData = new WebServiceOperation(WebServiceOperationName.createUpdateData, modelCRUD, loginRequest);
		SOAP11Request<StandardResponse> request = requestFactory.buildRequest(endpointAddress, createUpdateData, SOAP_ACTION,
				StandardResponse.class);
		request.execute(observer);
		requests.add(request);
		observers.add(observer);
	}

	public void executeDeleteData(ModelCRUD modelCRUD, SOAP11Observer<StandardResponse> observer) {
		WebServiceOperation deleteData = new WebServiceOperation(WebServiceOperationName.deleteData, modelCRUD, loginRequest);
		SOAP11Request<StandardResponse> request = requestFactory.buildRequest(endpointAddress, deleteData, SOAP_ACTION,
				StandardResponse.class);
		request.execute(observer);
		requests.add(request);
		observers.add(observer);
	}

	public void executeUpdateData(ModelCRUD modelCRUD, SOAP11Observer<StandardResponse> observer) {
		WebServiceOperation updateData = new WebServiceOperation(WebServiceOperationName.updateData, modelCRUD, loginRequest);
		SOAP11Request<StandardResponse> request = requestFactory.buildRequest(endpointAddress, updateData, SOAP_ACTION,
				StandardResponse.class);
		request.execute(observer);
		requests.add(request);
		observers.add(observer);
	}

	public boolean isExecuting() {
		boolean isExecuting = false;

		for (Iterator<SOAP11ListRequest<?>> iterator = listRequests.iterator(); iterator.hasNext();) {
			SOAP11ListRequest<?> req = iterator.next();
			if (req.isExecuting())
				isExecuting = true;
			if (req.isComplete())
				iterator.remove();
		}
		for (Iterator<SOAP11Request<?>> iterator = requests.iterator(); iterator.hasNext();) {
			SOAP11Request<?> req = iterator.next();
			if (req.isExecuting())
				isExecuting = true;
			if (req.isComplete())
				iterator.remove();
		}
		return isExecuting;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void cancelRequests() {
		for (SOAP11ListRequest req : listRequests) {
			req.cancel();
			for (SOAP11ListObserver observer : listObservers)
				req.deregisterObserver(observer);
		}
		for (SOAP11Request req : requests) {
			req.cancel();
			for (SOAP11Observer observer : observers)
				req.deregisterObserver(observer);
		}
		listRequests.clear();
		requests.clear();
		listObservers.clear();
		observers.clear();
	}

}
