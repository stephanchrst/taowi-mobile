package org.evenos.webservice.util;

import org.evenos.webservice.enums.WebServiceOperationName;
import org.evenos.webservice.nodes.ADLoginRequest;
import org.evenos.webservice.nodes.ModelCRUD;
import org.evenos.webservice.nodes.ModelGetList;
import org.evenos.webservice.nodes.ModelRunProcess;
import org.evenos.webservice.nodes.ModelSetDocAction;

import com.alexgilleran.icesoap.envelope.impl.BaseSOAP11Envelope;
import com.alexgilleran.icesoap.xml.XMLParentNode;

public class WebServiceOperation extends BaseSOAP11Envelope {

	private static final String PREFIX = "_0";
	public static final String NAMESPACE = "http://idempiere.org/ADInterface/1_0";

	private XMLParentNode mainNode;

	public WebServiceOperation(WebServiceOperationName operationName, ModelCRUD modelCRUD, ADLoginRequest loginRequest) {
		declarePrefix(PREFIX, NAMESPACE);
		mainNode = getBody().addNode(NAMESPACE, operationName.name()).addNode(NAMESPACE, "ModelCRUDRequest");
		mainNode.addElement(modelCRUD);
		mainNode.addElement(loginRequest);
	}

	public WebServiceOperation(WebServiceOperationName operationName, ModelRunProcess modelRunProcess, ADLoginRequest loginRequest) {
		declarePrefix(PREFIX, NAMESPACE);
		mainNode = getBody().addNode(NAMESPACE, operationName.name()).addNode(NAMESPACE, "ModelRunProcessRequest");
		mainNode.addElement(modelRunProcess);
		mainNode.addElement(loginRequest);
	}

	public WebServiceOperation(WebServiceOperationName operationName, ModelGetList modelGetList, ADLoginRequest loginRequest) {
		declarePrefix(PREFIX, NAMESPACE);
		mainNode = getBody().addNode(NAMESPACE, operationName.name()).addNode(NAMESPACE, "ModelGetListRequest");
		mainNode.addElement(modelGetList);
		mainNode.addElement(loginRequest);
	}

	public WebServiceOperation(WebServiceOperationName operationName, ModelSetDocAction modelSetDocAction, ADLoginRequest loginRequest) {
		declarePrefix(PREFIX, NAMESPACE);
		mainNode = getBody().addNode(NAMESPACE, operationName.name()).addNode(NAMESPACE, "ModelSetDocActionRequest");
		mainNode.addElement(modelSetDocAction);
		mainNode.addElement(loginRequest);
	}
}
