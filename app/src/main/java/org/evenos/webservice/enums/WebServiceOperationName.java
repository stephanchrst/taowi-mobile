package org.evenos.webservice.enums;

public enum WebServiceOperationName {
	createData, createUpdateData, deleteData, getList, queryData, readData, runProcess, setDocAction, updateData;
}
