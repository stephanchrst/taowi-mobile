package org.evenos.webservice.enums;

public enum WebServiceAction {
	Read, Create, CreateUpdate, Update, Delete;
}
