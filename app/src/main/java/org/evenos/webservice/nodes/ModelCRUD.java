package org.evenos.webservice.nodes;

import java.util.List;

import org.evenos.webservice.enums.WebServiceAction;
import org.evenos.webservice.fields.DataField;
import org.evenos.webservice.fields.DataRow;
import org.evenos.webservice.util.WebServiceOperation;

import com.alexgilleran.icesoap.xml.XMLElement;
import com.alexgilleran.icesoap.xml.XMLParentNode;
import com.alexgilleran.icesoap.xml.impl.XMLParentNodeImpl;

public class ModelCRUD extends XMLParentNodeImpl {

	public ModelCRUD(String serviceType) {
		super(WebServiceOperation.NAMESPACE, "ModelCRUD");
		this.addTextNode(WebServiceOperation.NAMESPACE, "serviceType", serviceType);
	}
	
	public ModelCRUD(String serviceType, int RecordID){
		this(serviceType);
		this.addTextNode(WebServiceOperation.NAMESPACE, "RecordID", String.valueOf(RecordID));
	}

	public ModelCRUD(String serviceType, String tableName, int RecordID){
		this(serviceType);
		this.addTextNode(WebServiceOperation.NAMESPACE, "TableName", tableName);
		this.addTextNode(WebServiceOperation.NAMESPACE, "RecordID", String.valueOf(RecordID));
	}
	
	public ModelCRUD(String serviceType, String tableName, int recordID, String filter, WebServiceAction action, DataRow dataRow) {
		this(serviceType);
		
		this.addTextNode(WebServiceOperation.NAMESPACE, "TableName", tableName);
		this.addTextNode(WebServiceOperation.NAMESPACE, "RecordID", String.valueOf(recordID));
		this.addTextNode(WebServiceOperation.NAMESPACE, "Filter", filter);
		this.addTextNode(WebServiceOperation.NAMESPACE, "Action", action.name());

		if (dataRow != null) {
			XMLParentNode dataRowNode = this.addNode(WebServiceOperation.NAMESPACE, "DataRow");
			List<DataField> dataFields = dataRow.getDataFields();
			for (DataField field : dataFields) {
				XMLParentNode fieldNode = dataRowNode.addNode(WebServiceOperation.NAMESPACE, "field");
				fieldNode.addAttribute("", "column", field.getColumnName());
				fieldNode.addTextNode(WebServiceOperation.NAMESPACE, "val", field.getValue());
			}

		}
	}

	public void setDataRow(DataRow dataRow) {
		if (dataRow != null) {
			XMLElement dr = null;
			for (XMLElement element : this.getChildNodes()) {
				if (element instanceof XMLParentNode) {
					XMLParentNode node = (XMLParentNode) element;
					if (node.getName().equals("DataRow")) {
						dr = element;
					}
				}
			}
			if (dr != null)
				this.getChildNodes().remove(dr);

			XMLParentNode dataRowNode = this.addNode(WebServiceOperation.NAMESPACE, "DataRow");
			List<DataField> dataFields = dataRow.getDataFields();
			for (DataField field : dataFields) {
				XMLParentNode fieldNode = dataRowNode.addNode(WebServiceOperation.NAMESPACE, "field");
				fieldNode.addAttribute("", "column", field.getColumnName());
				fieldNode.addTextNode(WebServiceOperation.NAMESPACE, "val", field.getValue());
			}
		}
	}

	public void addDataField(DataField dataField) {
		if (dataField == null)
			return;

		XMLParentNode dataRowNode = null;
		for (XMLElement element : this.getChildNodes()) {
			if (element instanceof XMLParentNode) {
				XMLParentNode node = (XMLParentNode) element;
				if (node.getName().equals("DataRow")) {
					dataRowNode = node;
				}
			}
		}
		if (dataRowNode == null)
			dataRowNode = this.addNode(WebServiceOperation.NAMESPACE, "DataRow");

		XMLParentNode fieldNode = dataRowNode.addNode(WebServiceOperation.NAMESPACE, "field");
		fieldNode.addAttribute("", "column", dataField.getColumnName());
		fieldNode.addTextNode(WebServiceOperation.NAMESPACE, "val", dataField.getValue());
	}
}
