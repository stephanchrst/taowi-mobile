package org.evenos.webservice.nodes;

import java.util.List;

import org.evenos.webservice.fields.DataField;
import org.evenos.webservice.fields.ParamValues;
import org.evenos.webservice.util.WebServiceOperation;

import com.alexgilleran.icesoap.xml.XMLParentNode;
import com.alexgilleran.icesoap.xml.impl.XMLParentNodeImpl;

public class ModelRunProcess extends XMLParentNodeImpl {

	public ModelRunProcess(String serviceType) {
		super(WebServiceOperation.NAMESPACE, "ModelRunProcess");
		this.addTextNode(WebServiceOperation.NAMESPACE, "serviceType", serviceType);
	}

	public ModelRunProcess(String serviceType, ParamValues paramValues) {
		this(serviceType);

		if (paramValues != null) {
			XMLParentNode dataRowNode = this.addNode(WebServiceOperation.NAMESPACE, "ParamValues");
			List<DataField> dataFields = paramValues.getDataFields();
			for (DataField field : dataFields) {
				XMLParentNode fieldNode = dataRowNode.addNode(WebServiceOperation.NAMESPACE, "field");
				fieldNode.addAttribute("", "column", field.getColumnName());
				fieldNode.addTextNode(WebServiceOperation.NAMESPACE, "val", field.getValue());
			}
		}
	}

}
