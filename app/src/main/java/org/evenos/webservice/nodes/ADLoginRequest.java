package org.evenos.webservice.nodes;

import org.evenos.webservice.util.WebServiceOperation;

import com.alexgilleran.icesoap.xml.impl.XMLParentNodeImpl;

public class ADLoginRequest extends XMLParentNodeImpl{

	public ADLoginRequest(String user, String pass, String lang, String clientID, String roleID, String orgID, String warehouseID, String stage) {
		super(WebServiceOperation.NAMESPACE, "ADLoginRequest");
		
		this.addTextNode(WebServiceOperation.NAMESPACE, "user", user);
		this.addTextNode(WebServiceOperation.NAMESPACE, "pass", pass);
		this.addTextNode(WebServiceOperation.NAMESPACE, "lang", lang);
		this.addTextNode(WebServiceOperation.NAMESPACE, "ClientID", clientID);
		this.addTextNode(WebServiceOperation.NAMESPACE, "RoleID", roleID);
		this.addTextNode(WebServiceOperation.NAMESPACE, "OrgID", orgID);
		this.addTextNode(WebServiceOperation.NAMESPACE, "WarehouseID", warehouseID);
		this.addTextNode(WebServiceOperation.NAMESPACE, "stage", stage);
	}

}
