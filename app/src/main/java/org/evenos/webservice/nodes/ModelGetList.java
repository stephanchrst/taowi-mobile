package org.evenos.webservice.nodes;

import org.evenos.webservice.util.WebServiceOperation;

import com.alexgilleran.icesoap.xml.impl.XMLParentNodeImpl;

public class ModelGetList extends XMLParentNodeImpl {
	
	public ModelGetList(String serviceType) {
		super(WebServiceOperation.NAMESPACE, "ModelGetList");
		this.addTextNode(WebServiceOperation.NAMESPACE, "serviceType", serviceType);
	}

	public ModelGetList(String serviceType, String AD_Reference_ID) {
		this(serviceType);

		if (AD_Reference_ID != null && AD_Reference_ID.length() > 0) {
			this.addTextNode(WebServiceOperation.NAMESPACE, "AD_Reference_ID", AD_Reference_ID);
		}
	}

	public ModelGetList(String serviceType, String AD_Reference_ID, String Filter) {
		this(serviceType, AD_Reference_ID);
		if (Filter != null && Filter.length() > 0) {
			this.addTextNode(WebServiceOperation.NAMESPACE, "Filter", Filter);
		}
	}
}
