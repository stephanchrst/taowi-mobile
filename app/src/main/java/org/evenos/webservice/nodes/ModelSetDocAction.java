package org.evenos.webservice.nodes;

import org.evenos.webservice.util.WebServiceOperation;

import com.alexgilleran.icesoap.xml.impl.XMLParentNodeImpl;

public class ModelSetDocAction extends XMLParentNodeImpl {

	public ModelSetDocAction(String serviceType) {
		super(WebServiceOperation.NAMESPACE, "ModelSetDocAction");
		this.addTextNode(WebServiceOperation.NAMESPACE, "serviceType", serviceType);
	}

	public ModelSetDocAction(String serviceType, String recordID) {
		this(serviceType);
		this.addTextNode(WebServiceOperation.NAMESPACE, "recordID", recordID);
	}

	public ModelSetDocAction(String serviceType, String recordID, String docAction) {
		this(serviceType, recordID);
		this.addTextNode(WebServiceOperation.NAMESPACE, "docAction", docAction);
	}

	public ModelSetDocAction(String serviceType, String tableName, String recordID, String docAction) {
		this(serviceType, recordID, docAction);
		this.addTextNode(WebServiceOperation.NAMESPACE, "tableName", tableName);
	}
}
