package org.evenos.utils;

import org.evenos.webservice.nodes.ADLoginRequest;

public class TestUtils {
    public static String endPoint;
    public static String ports;

    // Login request di hardcode dulu
    public static ADLoginRequest getADLoginRequest() {
        return new ADLoginRequest("SuperUser", "System", "191", "11", "102", "11", "103", "0");
    }

    public static ADLoginRequest getADLoginRequest(String username, String password) {
        return new ADLoginRequest(username, password, "191", "11", "102", "11", "103", "0");
    }

    // Endpoint juga di hardcode dulu
    public static String getEndpoint() {
        return "http://192.168.0.100:9999/ADInterface/services/ModelADService";
    }

    public static String getEndpoint(String host, String port) {
        return "http://" + host + ":" + port + "/ADInterface/services/ModelADService";
    }

}
