package com.tobaconsulting.taowi_mobile.controller.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tobaconsulting.taowi_mobile.R;
import com.tobaconsulting.taowi_mobile.model.MPurchaseOrderHdr;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Alvin on 8/11/2016.
 */
public class APurchaseOrderHdr extends RecyclerView.Adapter<APurchaseOrderHdr.MyViewHolder> {

    public List<MPurchaseOrderHdr> LPurchaseOrderHdr;
    public OnItemClickListener onItemClickListener;

    public APurchaseOrderHdr() {

    }

    public interface OnItemClickListener {
        void onItemClick(MPurchaseOrderHdr mPurchaseOrderHdr);
    }

    public APurchaseOrderHdr(List<MPurchaseOrderHdr> LPurchaseOrderHdr) {
        this.LPurchaseOrderHdr = LPurchaseOrderHdr;
    }

    public APurchaseOrderHdr(List<MPurchaseOrderHdr> LPurchaseOrderHdr, OnItemClickListener onItemClickListener) {
        this.LPurchaseOrderHdr = LPurchaseOrderHdr;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_purchase_order, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(LPurchaseOrderHdr.get(position), onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return LPurchaseOrderHdr.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.textDocNo)
        public TextView docNo;

        @Bind(R.id.textDateOrdered)
        public TextView dateOrdered;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final MPurchaseOrderHdr mPurchaseOrderHdr, final OnItemClickListener onItemClickListener) {
            docNo.setText(mPurchaseOrderHdr.getDocumentNo());
            dateOrdered.setText(mPurchaseOrderHdr.getDateOrdered());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(mPurchaseOrderHdr);
                }
            });
        }
    }
}
