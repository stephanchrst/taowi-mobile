package com.tobaconsulting.taowi_mobile.controller.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tobaconsulting.taowi_mobile.R;
import com.tobaconsulting.taowi_mobile.model.MPoSoLineHdr;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by alvinoktavianus on 8/25/2016.
 */
public class APoSoHdr extends RecyclerView.Adapter<APoSoHdr.MyViewHolder> {

    public List<MPoSoLineHdr> mPoSoLineHdrs;
    public onItemClickListener onItemClickListener;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_po_so_line, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.Bind(mPoSoLineHdrs.get(position), onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return mPoSoLineHdrs.size();
    }

    public interface onItemClickListener {
        void onItemClick(MPoSoLineHdr mPoSoLineHdr);
    }

    public APoSoHdr(List<MPoSoLineHdr> mPoSoLineHdrs, APoSoHdr.onItemClickListener onItemClickListener) {
        this.mPoSoLineHdrs = mPoSoLineHdrs;
        this.onItemClickListener = onItemClickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.textPoProduct)
        public TextView Product;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void Bind(final MPoSoLineHdr mPoSoLineHdr, final onItemClickListener onItemClickListener) {
            Product.setText(mPoSoLineHdr.getProduct());
            Product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(mPoSoLineHdr);
                }
            });
        }
    }
}
