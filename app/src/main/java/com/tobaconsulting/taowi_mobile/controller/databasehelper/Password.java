package com.tobaconsulting.taowi_mobile.controller.databasehelper;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by steven on 08/08/16.
 */
public class Password {

    private Context context;

    public Password(Context context) {
        this.context = context;
    }

    public void setPassword(String ipAddress) throws IOException {
        FileOutputStream outputStream = context.getApplicationContext().openFileOutput("password", Context.MODE_PRIVATE);
        outputStream.write(ipAddress.getBytes());
        outputStream.close();
    }

    public String getPassword() throws IOException {
        String password = "";
        int c;
        FileInputStream fileInputStream = context.getApplicationContext().openFileInput("password");
        while ((c = fileInputStream.read()) != -1) {
            password += Character.toString((char) c);
        }
        return password;
    }
}
