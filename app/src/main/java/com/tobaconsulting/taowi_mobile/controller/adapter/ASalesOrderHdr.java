package com.tobaconsulting.taowi_mobile.controller.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tobaconsulting.taowi_mobile.R;
import com.tobaconsulting.taowi_mobile.model.MSalesOrderHdr;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by steven on 15/08/16.
 */
public class ASalesOrderHdr extends RecyclerView.Adapter<ASalesOrderHdr.MyViewHolder> {

    public List<MSalesOrderHdr> LSalesOrderHdr;
    public OnItemClickListener onItemClickListener;

    public ASalesOrderHdr() {
    }

    public interface OnItemClickListener {
        void onItemClick(MSalesOrderHdr mSalesOrderHdr);
    }

    public ASalesOrderHdr(List<MSalesOrderHdr> LSalesOrderHdr) {
        this.LSalesOrderHdr = LSalesOrderHdr;
    }

    public ASalesOrderHdr(List<MSalesOrderHdr> LSalesOrderHdr, OnItemClickListener onItemClickListener) {
        this.LSalesOrderHdr = LSalesOrderHdr;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_sales_order, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       holder.bind(LSalesOrderHdr.get(position), onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return LSalesOrderHdr.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.textDocNoSales)
        public TextView docNo;

        @Bind(R.id.textDateOrderedSales)
        public TextView dateOrdered;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final MSalesOrderHdr mSalesOrderHdr, final OnItemClickListener onItemClickListener) {
            docNo.setText(mSalesOrderHdr.getDocumentNo());
            dateOrdered.setText(mSalesOrderHdr.getDateOrdered());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(mSalesOrderHdr);
                }
            });
        }
    }

}
