package com.tobaconsulting.taowi_mobile.controller.databasehelper;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by steven on 03/08/16.
 */

// HostAddress adalah Port
public class HostAddress {

    private Context context;

    public HostAddress(Context context) {
        this.context = context;
    }

    public void setHost(String host) throws IOException {
        FileOutputStream outputStream = context.getApplicationContext().openFileOutput("host", Context.MODE_PRIVATE);
        outputStream.write(host.getBytes());
        outputStream.close();
    }

    public String getHost() throws IOException {
        String host = "";
        int c;
        FileInputStream fileInputStream = context.getApplicationContext().openFileInput("host");
        while ((c = fileInputStream.read()) != -1) {
            host += Character.toString((char) c);
        }
        return host;
    }
}
