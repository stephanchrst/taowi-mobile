package com.tobaconsulting.taowi_mobile.controller.databasehelper;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by steven on 08/08/16.
 */
public class Username {
    private Context context;

    public Username(Context context) {
        this.context = context;
    }

    public void setUser(String host) throws IOException {
        FileOutputStream outputStream = context.getApplicationContext().openFileOutput("user", Context.MODE_PRIVATE);
        outputStream.write(host.getBytes());
        outputStream.close();
    }

    public String getUser() throws IOException {
        String user = "";
        int c;
        FileInputStream fileInputStream = context.getApplicationContext().openFileInput("user");
        while ((c = fileInputStream.read()) != -1) {
            user += Character.toString((char) c);
        }
        return user;
    }
}
