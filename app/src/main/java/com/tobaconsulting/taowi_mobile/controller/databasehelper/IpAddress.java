package com.tobaconsulting.taowi_mobile.controller.databasehelper;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by steven on 03/08/16.
 */
public class IpAddress {

    private Context context;

    public IpAddress(Context context) {
        this.context = context;
    }

    public void setIpAddress(String ipAddress) throws IOException {
        FileOutputStream outputStream = context.getApplicationContext().openFileOutput("ipaddress", Context.MODE_PRIVATE);
        outputStream.write(ipAddress.getBytes());
        outputStream.close();
    }

    public String getIpAddress() throws IOException {
        String ipaddress = "";
        int c;
        FileInputStream fileInputStream = context.getApplicationContext().openFileInput("ipaddress");
        while ((c = fileInputStream.read()) != -1) {
            ipaddress += Character.toString((char) c);
        }
        return ipaddress;
    }

}
