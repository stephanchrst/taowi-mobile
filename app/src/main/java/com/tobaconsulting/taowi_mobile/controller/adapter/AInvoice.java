package com.tobaconsulting.taowi_mobile.controller.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tobaconsulting.taowi_mobile.R;
import com.tobaconsulting.taowi_mobile.model.MInvoice;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Alvin on 8/8/2016.
 */
public class AInvoice extends RecyclerView.Adapter<AInvoice.MyViewHolder> {

    public List<MInvoice> LInvoice;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_invoice, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MInvoice mInvoice = LInvoice.get(position);
        holder.mColumn.setText(mInvoice.getColumn());
        holder.mValue.setText(mInvoice.getVal());
    }

    @Override
    public int getItemCount() {
        return LInvoice.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.textColumnValue)
        public TextView mColumn;

        @Bind(R.id.textValueValue)
        public TextView mValue;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public AInvoice(List<MInvoice> LInvoice) {
        this.LInvoice = LInvoice;
    }
}
