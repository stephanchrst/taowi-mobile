package com.tobaconsulting.taowi_mobile.model;

/**
 * Created by alvinoktavianus on 8/23/2016.
 */
public class MPoSoDtl {

    private String DocStatus;
    private Float GrandTotal;
    private String DocumentNo;
    private String DatePromised;
    private Float TotalLines;
    private String C_Campaign_ID;
    private String Description;
    private String DocumentType;
    private String DateOrdered;
    private String SalesRep_Name;
    private String TaxID;
    private Character PaymentRule;
    private String M_Warehouse_Name;
    private String M_Paymentterm_Value;
    private String Bp_Location_Name;
    private String cur_description;

    public MPoSoDtl() {
    }

    public MPoSoDtl(String docStatus, Float grandTotal, String documentNo, String datePromised, Float totalLines, String c_Campaign_ID, String description, String documentType, String dateOrdered, String salesRep_Name, String taxID, Character paymentRule, String m_Warehouse_Name, String m_Paymentterm_Value, String bp_Location_Name, String cur_description) {
        DocStatus = docStatus;
        GrandTotal = grandTotal;
        DocumentNo = documentNo;
        DatePromised = datePromised;
        TotalLines = totalLines;
        C_Campaign_ID = c_Campaign_ID;
        Description = description;
        DocumentType = documentType;
        DateOrdered = dateOrdered;
        SalesRep_Name = salesRep_Name;
        TaxID = taxID;
        PaymentRule = paymentRule;
        M_Warehouse_Name = m_Warehouse_Name;
        M_Paymentterm_Value = m_Paymentterm_Value;
        Bp_Location_Name = bp_Location_Name;
        this.cur_description = cur_description;
    }

    public String getDocStatus() {
        return DocStatus;
    }

    public void setDocStatus(String docStatus) {
        DocStatus = docStatus;
    }

    public Float getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(Float grandTotal) {
        GrandTotal = grandTotal;
    }

    public String getDocumentNo() {
        return DocumentNo;
    }

    public void setDocumentNo(String documentNo) {
        DocumentNo = documentNo;
    }

    public String getDatePromised() {
        return DatePromised;
    }

    public void setDatePromised(String datePromised) {
        DatePromised = datePromised;
    }

    public Float getTotalLines() {
        return TotalLines;
    }

    public void setTotalLines(Float totalLines) {
        TotalLines = totalLines;
    }

    public String getC_Campaign_ID() {
        return C_Campaign_ID;
    }

    public void setC_Campaign_ID(String c_Campaign_ID) {
        C_Campaign_ID = c_Campaign_ID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDocumentType() {
        return DocumentType;
    }

    public void setDocumentType(String documentType) {
        DocumentType = documentType;
    }

    public String getDateOrdered() {
        return DateOrdered;
    }

    public void setDateOrdered(String dateOrdered) {
        DateOrdered = dateOrdered;
    }

    public String getSalesRep_Name() {
        return SalesRep_Name;
    }

    public void setSalesRep_Name(String salesRep_Name) {
        SalesRep_Name = salesRep_Name;
    }

    public String getTaxID() {
        return TaxID;
    }

    public void setTaxID(String taxID) {
        TaxID = taxID;
    }

    public Character getPaymentRule() {
        return PaymentRule;
    }

    public void setPaymentRule(Character paymentRule) {
        PaymentRule = paymentRule;
    }

    public String getM_Warehouse_Name() {
        return M_Warehouse_Name;
    }

    public void setM_Warehouse_Name(String m_Warehouse_Name) {
        M_Warehouse_Name = m_Warehouse_Name;
    }

    public String getM_Paymentterm_Value() {
        return M_Paymentterm_Value;
    }

    public void setM_Paymentterm_Value(String m_Paymentterm_Value) {
        M_Paymentterm_Value = m_Paymentterm_Value;
    }

    public String getBp_Location_Name() {
        return Bp_Location_Name;
    }

    public void setBp_Location_Name(String bp_Location_Name) {
        Bp_Location_Name = bp_Location_Name;
    }

    public String getCur_description() {
        return cur_description;
    }

    public void setCur_description(String cur_description) {
        this.cur_description = cur_description;
    }

}
