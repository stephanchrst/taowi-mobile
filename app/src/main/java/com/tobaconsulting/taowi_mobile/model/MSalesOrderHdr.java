package com.tobaconsulting.taowi_mobile.model;

/**
 * Created by steven on 15/08/16.
 */
public class MSalesOrderHdr {

    private String DocumentNo;
    private String DateOrdered;
    private String C_Order_ID;

    public MSalesOrderHdr() {
    }

    public MSalesOrderHdr(String documentNo, String dateOrdered, String c_order_id) {
        DocumentNo = documentNo;
        DateOrdered = dateOrdered;
        C_Order_ID = c_order_id;
    }

    public String getC_Order_ID() {
        return C_Order_ID;
    }

    public void setC_Order_ID(String c_Order_ID) {
        C_Order_ID = c_Order_ID;
    }

    public String getDocumentNo() {
        return DocumentNo;
    }

    public void setDocumentNo(String documentNo) {
        DocumentNo = documentNo;
    }

    public String getDateOrdered() {
        return DateOrdered;
    }

    public void setDateOrdered(String dateOrdered) {
        DateOrdered = dateOrdered;
    }
}
