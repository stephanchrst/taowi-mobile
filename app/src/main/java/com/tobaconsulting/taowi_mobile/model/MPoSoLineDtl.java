package com.tobaconsulting.taowi_mobile.model;

/**
 * Created by alvinoktavianus on 8/30/2016.
 */
public class MPoSoLineDtl {

    public String AD_Org_ID;
    public int Discount;
    public String product_name;
    public int Line;
    public int QtyEntered;
    public int QtyOrdered;
    public float PriceEntered;
    public int PriceList;

    public MPoSoLineDtl() {
    }

    public MPoSoLineDtl(String AD_Org_ID, int discount, String product_name, int line, int qtyEntered, int qtyOrdered, float priceEntered, int priceList) {
        this.AD_Org_ID = AD_Org_ID;
        Discount = discount;
        this.product_name = product_name;
        Line = line;
        QtyEntered = qtyEntered;
        QtyOrdered = qtyOrdered;
        PriceEntered = priceEntered;
        PriceList = priceList;
    }

    public String getAD_Org_ID() {
        return AD_Org_ID;
    }

    public void setAD_Org_ID(String AD_Org_ID) {
        this.AD_Org_ID = AD_Org_ID;
    }

    public int getDiscount() {
        return Discount;
    }

    public void setDiscount(int discount) {
        Discount = discount;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getLine() {
        return Line;
    }

    public void setLine(int line) {
        Line = line;
    }

    public int getQtyEntered() {
        return QtyEntered;
    }

    public void setQtyEntered(int qtyEntered) {
        QtyEntered = qtyEntered;
    }

    public int getQtyOrdered() {
        return QtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        QtyOrdered = qtyOrdered;
    }

    public float getPriceEntered() {
        return PriceEntered;
    }

    public void setPriceEntered(float priceEntered) {
        PriceEntered = priceEntered;
    }

    public int getPriceList() {
        return PriceList;
    }

    public void setPriceList(int priceList) {
        PriceList = priceList;
    }
}
