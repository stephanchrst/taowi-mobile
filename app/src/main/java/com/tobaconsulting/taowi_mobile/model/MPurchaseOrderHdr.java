package com.tobaconsulting.taowi_mobile.model;

/**
 * Created by Alvin on 8/11/2016.
 */
public class MPurchaseOrderHdr {

    private String DocumentNo;
    private String DateOrdered;
    private String C_Order_ID;

    public MPurchaseOrderHdr(String documentNo, String dateOrdered, String c_Order_ID) {

        DocumentNo = documentNo;
        DateOrdered = dateOrdered;
        C_Order_ID = c_Order_ID;
    }

    public MPurchaseOrderHdr() {
    }

    public MPurchaseOrderHdr(String documentNo, String dateOrdered) {
        DocumentNo = documentNo;
        DateOrdered = dateOrdered;
    }

    public String getC_Order_ID() {
        return C_Order_ID;
    }

    public void setC_Order_ID(String c_Order_ID) {
        C_Order_ID = c_Order_ID;
    }

    public String getDocumentNo() {
        return DocumentNo;
    }

    public void setDocumentNo(String documentNo) {
        DocumentNo = documentNo;
    }

    public String getDateOrdered() {
        return DateOrdered;
    }

    public void setDateOrdered(String dateOrdered) {
        DateOrdered = dateOrdered;
    }
}
