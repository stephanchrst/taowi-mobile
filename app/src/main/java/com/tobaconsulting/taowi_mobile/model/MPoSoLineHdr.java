package com.tobaconsulting.taowi_mobile.model;

/**
 * Created by alvinoktavianus on 8/25/2016.
 */
public class MPoSoLineHdr {

    private String Product;
    private String C_Orderline_ID;

    public MPoSoLineHdr(String product, String c_Orderline_ID) {
        Product = product;
        C_Orderline_ID = c_Orderline_ID;
    }

    public MPoSoLineHdr() {
    }

    public MPoSoLineHdr(String product) {
        Product = product;
    }

    public String getProduct() {
        return Product;
    }

    public void setProduct(String product) {
        Product = product;
    }

    public String getC_Orderline_ID() {
        return C_Orderline_ID;
    }

    public void setC_Orderline_ID(String c_Orderline_ID) {
        C_Orderline_ID = c_Orderline_ID;
    }

}
