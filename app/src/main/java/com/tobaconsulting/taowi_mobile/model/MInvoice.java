package com.tobaconsulting.taowi_mobile.model;

/**
 * Created by Alvin on 8/8/2016.
 */
public class MInvoice {

    private String column;
    private String val;

    public MInvoice() {
    }

    public MInvoice(String column, String val) {
        this.column = column;
        this.val = val;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
