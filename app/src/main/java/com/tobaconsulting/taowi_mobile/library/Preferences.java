package com.tobaconsulting.taowi_mobile.library;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by alvinoktavianus on 8/19/2016.
 */
public class Preferences {

    public static String getIpAddress(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString("ipaddress", "");
    }

    public static String getPort(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString("port", "");
    }

}
