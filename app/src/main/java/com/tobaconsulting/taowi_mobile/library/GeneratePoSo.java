package com.tobaconsulting.taowi_mobile.library;

import com.tobaconsulting.taowi_mobile.model.MPoSoDtl;
import com.tobaconsulting.taowi_mobile.model.MPoSoLineDtl;
import com.tobaconsulting.taowi_mobile.model.MPoSoLineHdr;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alvinoktavianus on 8/23/2016.
 */
public class GeneratePoSo {

    public static List<MPoSoDtl> generatePoSoDtl(NodeList nodeList) {
        NodeList list = nodeList.item(0).getChildNodes();
        List<MPoSoDtl> mPoSoDtls = new ArrayList<>();
        MPoSoDtl mPoSoDtl = new MPoSoDtl();
        for (int i = 0; i < list.getLength(); i++) {
            Element element = (Element) list.item(i);
            String value = element.getElementsByTagName("val").item(0).getTextContent();
            switch (element.getAttribute("column")) {
                case "DocStatus":
                    mPoSoDtl.setDocStatus(value);
                    break;
                case "GrandTotal":
                    mPoSoDtl.setGrandTotal(Float.parseFloat(value));
                    break;
                case "DocumentNo":
                    mPoSoDtl.setDocumentNo(value);
                    break;
                case "DatePromised":
                    mPoSoDtl.setDatePromised(value);
                    break;
                case "DocumentType":
                    mPoSoDtl.setDocumentType(value);
                    break;
                case "TotalLines":
                    mPoSoDtl.setTotalLines(Float.parseFloat(value));
                    break;
                case "DateOrdered":
                    mPoSoDtl.setDateOrdered(value);
                    break;
                case "SalesRep_Name":
                    mPoSoDtl.setSalesRep_Name(value);
                    break;
                case "TaxID":
                    mPoSoDtl.setTaxID(value);
                    break;
                case "PaymentRule":
                    mPoSoDtl.setPaymentRule(value.charAt(0));
                    break;
                case "m_warehouse_name":
                    mPoSoDtl.setM_Warehouse_Name(value);
                    break;
                case "m_paymentterm_value":
                    mPoSoDtl.setM_Paymentterm_Value(value);
                    break;
                case "bp_location_name":
                    mPoSoDtl.setBp_Location_Name(value);
                    break;
                case "cur_description":
                    mPoSoDtl.setCur_description(value);
                    break;
            }
        }
        mPoSoDtls.add(mPoSoDtl);
        return mPoSoDtls;
    }

    public static List<MPoSoLineHdr> generatePoSoLineHdr(NodeList nodeList) {
        NodeList list = nodeList.item(0).getChildNodes();
        List<MPoSoLineHdr> mPoSoLineHdrs = new ArrayList<>();
        MPoSoLineHdr mPoSoLineHdr = new MPoSoLineHdr();
        for (int i = 0; i < list.getLength(); i++) {
            Element element = (Element) list.item(i);
            String value = element.getElementsByTagName("val").item(0).getTextContent();
            switch (element.getAttribute("column")) {
                case "Name":
                    mPoSoLineHdr.setProduct(value);
                    break;
                case "C_OrderLine_ID":
                    mPoSoLineHdr.setC_Orderline_ID(value);
                    break;
            }
        }
        if (!mPoSoLineHdr.getC_Orderline_ID().equals(""))
            mPoSoLineHdrs.add(mPoSoLineHdr);
        return mPoSoLineHdrs;
    }

    public static List<MPoSoLineDtl> generatePoSoLineDtl(NodeList nodeList) {
        NodeList list = nodeList.item(0).getChildNodes();
        List<MPoSoLineDtl> mPoSoLineDtls = new ArrayList<>();
        MPoSoLineDtl mPoSoLineDtl = new MPoSoLineDtl();
        for (int i = 0; i < list.getLength(); i++) {
            Element element = (Element) list.item(i);
            String value = element.getElementsByTagName("val").item(0).getTextContent();
            switch (element.getAttribute("column")) {
                case "AD_Org_ID":
                    mPoSoLineDtl.setAD_Org_ID(value);
                    break;
                case "Discount":
                    mPoSoLineDtl.setDiscount(Integer.parseInt(value));
                    break;
                case "product_name":
                    mPoSoLineDtl.setProduct_name(value);
                    break;
                case "Line":
                    mPoSoLineDtl.setLine(Integer.parseInt(value));
                    break;
                case "QtyEntered":
                    mPoSoLineDtl.setQtyEntered(Integer.parseInt(value));
                    break;
                case "QtyOrdered":
                    mPoSoLineDtl.setQtyOrdered(Integer.parseInt(value));
                    break;
                case "PriceEntered":
                    mPoSoLineDtl.setPriceEntered(Float.parseFloat(value));
                    break;
                case "PriceList":
                    mPoSoLineDtl.setPriceList(Integer.parseInt(value));
                    break;
            }
        }
        mPoSoLineDtls.add(mPoSoLineDtl);
        return mPoSoLineDtls;
    }

}
