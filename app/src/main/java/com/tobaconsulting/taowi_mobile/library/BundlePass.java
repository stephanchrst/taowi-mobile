package com.tobaconsulting.taowi_mobile.library;

import android.os.Bundle;

/**
 * Created by alvinoktavianus on 8/18/2016.
 */
public class BundlePass {

    public static String getIntentExtra(Bundle bundle, String key) {
        return bundle.getString(key);
    }

}
