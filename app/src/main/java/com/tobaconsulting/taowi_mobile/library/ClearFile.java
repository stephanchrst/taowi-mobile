package com.tobaconsulting.taowi_mobile.library;

import android.content.Context;

import com.tobaconsulting.taowi_mobile.controller.databasehelper.HostAddress;
import com.tobaconsulting.taowi_mobile.controller.databasehelper.IpAddress;
import com.tobaconsulting.taowi_mobile.controller.databasehelper.Password;
import com.tobaconsulting.taowi_mobile.controller.databasehelper.Username;

import java.io.IOException;

/**
 * Created by Alvin on 8/9/2016.
 */
public class ClearFile {

    public Context context;

    public ClearFile() {
    }

    public ClearFile(Context context) {
        this.context = context;
    }

    public void clearFile() throws IOException {
        new Username(context).setUser("");
        new Password(context).setPassword("");
        new HostAddress(context).setHost("");
        new IpAddress(context).setIpAddress("");
    }
}
