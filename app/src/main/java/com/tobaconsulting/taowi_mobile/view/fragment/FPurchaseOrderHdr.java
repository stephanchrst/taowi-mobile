package com.tobaconsulting.taowi_mobile.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.tobaconsulting.taowi_mobile.R;
import com.tobaconsulting.taowi_mobile.controller.adapter.APurchaseOrderHdr;
import com.tobaconsulting.taowi_mobile.library.BundlePass;
import com.tobaconsulting.taowi_mobile.library.ParseXML;
import com.tobaconsulting.taowi_mobile.library.Preferences;
import com.tobaconsulting.taowi_mobile.model.MPurchaseOrderHdr;
import com.tobaconsulting.taowi_mobile.query.QPoSoHdr;
import com.tobaconsulting.taowi_mobile.view.DividerItemDecoration;
import com.tobaconsulting.taowi_mobile.view.activity.PurchaseOrderDtlActivity;

import org.evenos.utils.TestUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FPurchaseOrderHdr extends Fragment {

    public static final String TAG = FPurchaseOrderHdr.class.getSimpleName();
    public static final MediaType MEDIA_TYPE = MediaType.parse("text/x-markdown; charset=utf-8");

    public List<MPurchaseOrderHdr> mPurchaseOrderHdrs = new ArrayList<>();

    @Bind(R.id.recycler_po)
    public RecyclerView recyclerView;

    @Bind(R.id.pbPO)
    public ProgressBar progressBar;

    public FPurchaseOrderHdr() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_purchase_order, container, false);

        ButterKnife.bind(this, view);

        String ipaddress = Preferences.getIpAddress(getContext());
        String port = Preferences.getPort(getContext());
        String url = TestUtils.getEndpoint(ipaddress, port);
        String username = BundlePass.getIntentExtra(getActivity().getIntent().getExtras(), "username");
        String password = BundlePass.getIntentExtra(getActivity().getIntent().getExtras(), "password");
        String data = QPoSoHdr.QueryPO("view_purchase_order", username, password);

        RequestBody requestBody = RequestBody.create(MEDIA_TYPE, data);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Content-Type", "application/xml")
                .post(requestBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        String data = response.body().string();
                        Log.i(TAG, "Response : " + data);
                        Document document = ParseXML.convertStringToDocument(data);
                        document.getDocumentElement().normalize();
                        NodeList nodeList = document.getElementsByTagName("DataRow");
                        ProcessXML(nodeList);

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.INVISIBLE);
                                recyclerView.setVisibility(View.VISIBLE);
                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                                recyclerView.setLayoutManager(layoutManager);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
                                recyclerView.setAdapter(new APurchaseOrderHdr(mPurchaseOrderHdrs, new APurchaseOrderHdr.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(MPurchaseOrderHdr mPurchaseOrderHdr) {
                                        Intent intent = new Intent(getActivity(), PurchaseOrderDtlActivity.class);
                                        intent.putExtra("documentNumber", mPurchaseOrderHdr.getDocumentNo());
                                        intent.putExtra("C_Order_ID", mPurchaseOrderHdr.getC_Order_ID());
                                        intent.putExtra("username", BundlePass.getIntentExtra(getActivity().getIntent().getExtras(), "username"));
                                        intent.putExtra("password", BundlePass.getIntentExtra(getActivity().getIntent().getExtras(), "password"));
                                        startActivity(intent);
                                    }
                                }));
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    response.close();
                }
            }
        });

        return view;
    }

    private void ProcessXML(NodeList nodeList) {
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            NodeList list = node.getChildNodes();
            String c_order_id = "", docNo = "", dateOrdered = "";
            for (int j = 0; j < list.getLength(); j++) {
                Element element = (Element) list.item(j);
                switch (element.getAttribute("column")) {
                    case "C_Order_ID":
                        c_order_id = element.getElementsByTagName("val").item(0).getTextContent();
                        break;
                    case "DocumentNo":
                        docNo = element.getElementsByTagName("val").item(0).getTextContent();
                        break;
                    case "DateOrdered":
                        dateOrdered = element.getElementsByTagName("val").item(0).getTextContent();
                        break;
                }

            }
            MPurchaseOrderHdr mPurchaseOrderHdr = new MPurchaseOrderHdr(docNo, dateOrdered, c_order_id);
            mPurchaseOrderHdrs.add(mPurchaseOrderHdr);
        }
    }

}
