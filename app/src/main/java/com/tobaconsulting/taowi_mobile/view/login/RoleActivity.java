package com.tobaconsulting.taowi_mobile.view.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tobaconsulting.taowi_mobile.R;
import com.tobaconsulting.taowi_mobile.library.BundlePass;
import com.tobaconsulting.taowi_mobile.view.MainActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RoleActivity extends AppCompatActivity implements View.OnClickListener {

    @Bind(R.id.editTextClient)
    public EditText mClient;

    @Bind(R.id.editTextRole)
    public EditText mRole;

    @Bind(R.id.editTextOrg)
    public EditText mOrg;

    @Bind(R.id.editTextWarehouse)
    public EditText mWarehouse;

    @Bind(R.id.buttonOk)
    public Button mOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_role);

        ButterKnife.bind(this);
        mClient.setOnClickListener(this);
        mRole.setOnClickListener(this);
        mOrg.setOnClickListener(this);
        mWarehouse.setOnClickListener(this);
        mOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.editTextClient:
                openClient();
                break;
            case R.id.editTextRole:
                openRole();
                break;
            case R.id.editTextOrg:
                openOrg();
                break;
            case R.id.editTextWarehouse:
                openWarehouse();
                break;
            case R.id.buttonOk:
                okButtonListener();
                break;
        }
    }

    public void okButtonListener() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("username", BundlePass.getIntentExtra(getIntent().getExtras(), "username"));
        intent.putExtra("password", BundlePass.getIntentExtra(getIntent().getExtras(), "password"));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void openWarehouse() {
        final CharSequence items[] = new CharSequence[]{};
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Select Warehouse")
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void openOrg() {
        final CharSequence items[] = new CharSequence[]{"*", "Cabang", "Color, Inc", "Fertilizer", "Furniture"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Select Org")
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                mOrg.setText(items[0]);
                                break;
                            case 1:
                                mOrg.setText(items[1]);
                                break;
                            case 2:
                                mOrg.setText(items[2]);
                                break;
                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void openRole() {
        final CharSequence items[] = new CharSequence[]{"GardenWorld Admin", "GardenWorld User", "Limited Access", "Production", "Web Service Execution"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Select Role")
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                mRole.setText(items[0]);
                                break;
                            case 1:
                                mRole.setText(items[1]);
                                break;
                            case 2:
                                mRole.setText(items[2]);
                                break;
                            case 3:
                                mRole.setText(items[3]);
                                break;
                            case 4:
                                mRole.setText(items[4]);
                                break;
                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void openClient() {
        final CharSequence items[] = new CharSequence[]{"GardenWorld", "HDGroup", "System"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Select Client")
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                mClient.setText(items[0]);
                                break;
                            case 1:
                                mClient.setText(items[1]);
                                break;
                            case 2:
                                mClient.setText(items[2]);
                                break;
                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
