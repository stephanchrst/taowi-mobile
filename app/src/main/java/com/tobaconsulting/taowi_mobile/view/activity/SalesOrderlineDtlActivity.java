package com.tobaconsulting.taowi_mobile.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tobaconsulting.taowi_mobile.R;
import com.tobaconsulting.taowi_mobile.library.BundlePass;
import com.tobaconsulting.taowi_mobile.library.GeneratePoSo;
import com.tobaconsulting.taowi_mobile.library.ParseXML;
import com.tobaconsulting.taowi_mobile.library.Preferences;
import com.tobaconsulting.taowi_mobile.model.MPoSoLineDtl;
import com.tobaconsulting.taowi_mobile.query.QPoSoLineDtl;

import org.evenos.utils.TestUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by alvinoktavianus on 8/25/2016.
 */
public class SalesOrderlineDtlActivity extends AppCompatActivity {

    public static final String TAG = SalesOrderlineDtlActivity.class.getSimpleName();
    public static final MediaType MEDIA_TYPE = MediaType.parse("text/x-markdown; charset=utf-8");
    private List<MPoSoLineDtl> mPoSoLineDtls = new ArrayList<>();

    @Bind(R.id.textPoSo)
    public TextView POSO;

    @Bind(R.id.textLineNo)
    public TextView lineNo;

    @Bind(R.id.txtProduct)
    public TextView product;

    @Bind(R.id.textSetInstance)
    public TextView instance;

    @Bind(R.id.textOrganizationLine)
    public TextView organization;

    @Bind(R.id.textDescription)
    public TextView description;

    @Bind(R.id.textQuantity)
    public TextView quantity;

    @Bind(R.id.textUOM)
    public TextView uom;

    @Bind(R.id.textPoSoQuantity)
    public TextView poSoQuantity;

    @Bind(R.id.textPrice)
    public TextView price;

    @Bind(R.id.textFieldPrice)
    public TextView listPrice;

    @Bind(R.id.textDiscountLine)
    public TextView discount;

    @Bind(R.id.po_so_layout_wrapper)
    public LinearLayout linearLayout;

    @Bind(R.id.pb_po_so_dtl)
    public ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_po_so_line_dtl);

        ButterKnife.bind(this);

        String ipaddress = Preferences.getIpAddress(getBaseContext());
        String port = Preferences.getPort(getBaseContext());
        String url = TestUtils.getEndpoint(ipaddress, port);
        String C_Orderline_ID = BundlePass.getIntentExtra(getIntent().getExtras(), "C_Orderline_ID");
        Log.i(TAG, "onCreate: " + C_Orderline_ID);
        String username = BundlePass.getIntentExtra(getIntent().getExtras(), "username");
        String password = BundlePass.getIntentExtra(getIntent().getExtras(), "password");

        OkHttpClient client = new OkHttpClient();

        String query = QPoSoLineDtl.GeneratePoSoLineDtl("po_so_line_dtl", username, password, C_Orderline_ID);

        RequestBody requestBody = RequestBody.create(MEDIA_TYPE, query);

        Request request = new Request.Builder()
                .url(url)
                .addHeader("Content-Type", "application/xml")
                .post(requestBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String data = response.body().string();
                    if (response.isSuccessful()) {
                        Log.i(TAG, "onResponse: " + data);
                        Document document = ParseXML.convertStringToDocument(data);
                        document.getDocumentElement().normalize();
                        NodeList nodeList = document.getElementsByTagName("DataRow");
                        mPoSoLineDtls = GeneratePoSo.generatePoSoLineDtl(nodeList);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                POSO.setText("SO Line");
                                lineNo.append(" " + String.valueOf(mPoSoLineDtls.get(0).getLine()));
                                product.append(" " + mPoSoLineDtls.get(0).getProduct_name());
                                quantity.append(" " + String.valueOf(mPoSoLineDtls.get(0).getQtyEntered()));
                                poSoQuantity.setText("SO Quantity : " + String.valueOf(mPoSoLineDtls.get(0).getQtyOrdered()));
                                price.append(" " + String.valueOf(mPoSoLineDtls.get(0).getPriceEntered()));
                                listPrice.append(" " + String.valueOf(mPoSoLineDtls.get(0).getPriceList()));
                                discount.append(" " + String.valueOf(mPoSoLineDtls.get(0).getDiscount()));
                                progressBar.setVisibility(View.INVISIBLE);
                                linearLayout.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    response.close();
                }
            }
        });
    }
}
