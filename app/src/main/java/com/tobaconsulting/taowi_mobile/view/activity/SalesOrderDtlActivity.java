package com.tobaconsulting.taowi_mobile.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tobaconsulting.taowi_mobile.R;
import com.tobaconsulting.taowi_mobile.library.BundlePass;
import com.tobaconsulting.taowi_mobile.library.GeneratePoSo;
import com.tobaconsulting.taowi_mobile.library.ParseXML;
import com.tobaconsulting.taowi_mobile.library.Preferences;
import com.tobaconsulting.taowi_mobile.model.MPoSoDtl;
import com.tobaconsulting.taowi_mobile.query.QPoSoDtl;

import org.evenos.utils.TestUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SalesOrderDtlActivity extends AppCompatActivity {

    public static final String TAG = SalesOrderDtlActivity.class.getSimpleName();
    public static final MediaType MEDIA_TYPE = MediaType.parse("text/x-markdown; charset=utf-8");
    public List<MPoSoDtl> mPoSoDtls = new ArrayList<>();

    @Bind(R.id.pbDtl)
    public ProgressBar progressBar;

    @Bind(R.id.layout_wrapper)
    public LinearLayout linearLayout;

    @Bind(R.id.txtPoSo)
    public TextView SO;

    @Bind(R.id.txtDateOrdered)
    public TextView dateOrdered;

    @Bind(R.id.txtClient)
    public TextView client;

    @Bind(R.id.txtOrganization)
    public TextView organization;

    @Bind(R.id.txtDocNumber)
    public TextView documentNo;

    @Bind(R.id.txtDocType)
    public TextView targetDocumentType;

    @Bind(R.id.txtDesciption)
    public TextView description;

    @Bind(R.id.txtDatePromised)
    public TextView datePromised;

    @Bind(R.id.txtBusinessPartner)
    public TextView businessPartner;

    @Bind(R.id.txtPartnerLocation)
    public TextView partnerLocation;

    @Bind(R.id.txtUserContact)
    public TextView userContact;

    @Bind(R.id.txtInvoicePartner)
    public TextView invoicePartner;

    @Bind(R.id.txtInvoiceContact)
    public TextView invoiceContact;

    @Bind(R.id.txtInvoiceLocation)
    public TextView invoiceLocation;

    @Bind(R.id.txtWarehouse)
    public TextView warehouse;

    @Bind(R.id.txtPriceList)
    public TextView priceList;

    @Bind(R.id.txtPaymentTerm)
    public TextView paymentTerm;

    @Bind(R.id.txtPaymentRule)
    public TextView paymentRule;

    @Bind(R.id.txtTax)
    public TextView Tax;

    @Bind(R.id.txtCurrency)
    public TextView currency;

    @Bind(R.id.txtCompanyAgent)
    public TextView companyAgent;

    @Bind(R.id.txtProject)
    public TextView projectReference;

    @Bind(R.id.txtCampaign)
    public TextView campaign;

    @Bind(R.id.txtDocumentStatus)
    public TextView documentStatus;

    @Bind(R.id.txtTotalLines)
    public TextView totalLines;

    @Bind(R.id.txtDocumentType)
    public TextView documentType;

    @Bind(R.id.txtGrandTotal)
    public TextView grandTotal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_po_so_dtl);

        ButterKnife.bind(this);

        String c_order_id = BundlePass.getIntentExtra(getIntent().getExtras(), "C_Order_ID");
        String username = BundlePass.getIntentExtra(getIntent().getExtras(), "username");
        String password = BundlePass.getIntentExtra(getIntent().getExtras(), "password");

        String data = QPoSoDtl.QuerySoDtl("purchase_order_dtl", username, password, c_order_id);

        RequestBody requestBody = RequestBody.create(MEDIA_TYPE, data);

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(TestUtils.getEndpoint(Preferences.getIpAddress(this), Preferences.getPort(this)))
                .addHeader("Content-Type", "application/xml")
                .post(requestBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        String data = response.body().string();
                        Log.i(TAG, "Response : " + data);
                        Document document = ParseXML.convertStringToDocument(data);
                        document.getDocumentElement().normalize();
                        NodeList nodeList = document.getElementsByTagName("DataRow");
                        mPoSoDtls = GeneratePoSo.generatePoSoDtl(nodeList);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                SO.setText("Sales Order : " + mPoSoDtls.get(0).getDocumentNo());
                                dateOrdered.append(" " + mPoSoDtls.get(0).getDateOrdered());
                                documentStatus.append(" " + mPoSoDtls.get(0).getDocStatus());
                                grandTotal.append(" " + String.valueOf(mPoSoDtls.get(0).getGrandTotal()));
                                documentNo.append(" " + mPoSoDtls.get(0).getDocumentNo());
                                datePromised.append(" " + mPoSoDtls.get(0).getDatePromised());
                                totalLines.append(" " + String.valueOf(mPoSoDtls.get(0).getTotalLines()));
                                documentType.append(" " + mPoSoDtls.get(0).getDocumentType());
                                companyAgent.append(" " + mPoSoDtls.get(0).getSalesRep_Name());
                                paymentRule.append(" " + String.valueOf(mPoSoDtls.get(0).getPaymentRule()));
                                warehouse.append(" " + mPoSoDtls.get(0).getM_Warehouse_Name());
                                paymentTerm.append(" " + mPoSoDtls.get(0).getM_Paymentterm_Value());
                                partnerLocation.append(" " + mPoSoDtls.get(0).getBp_Location_Name());
                                currency.append(" " + mPoSoDtls.get(0).getCur_description());
                                progressBar.setVisibility(View.INVISIBLE);
                                linearLayout.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    response.close();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.so_orderline, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.so_orderline:
                Intent intent = new Intent(SalesOrderDtlActivity.this, SalesOrderlineHdrActivity.class);
                intent.putExtra("C_Order_ID", BundlePass.getIntentExtra(getIntent().getExtras(), "C_Order_ID"));
                intent.putExtra("username", BundlePass.getIntentExtra(getIntent().getExtras(), "username"));
                intent.putExtra("password", BundlePass.getIntentExtra(getIntent().getExtras(), "password"));
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
