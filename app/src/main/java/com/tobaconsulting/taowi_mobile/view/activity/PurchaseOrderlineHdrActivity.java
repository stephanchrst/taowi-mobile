package com.tobaconsulting.taowi_mobile.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tobaconsulting.taowi_mobile.R;
import com.tobaconsulting.taowi_mobile.controller.adapter.APoSoHdr;
import com.tobaconsulting.taowi_mobile.library.BundlePass;
import com.tobaconsulting.taowi_mobile.library.GeneratePoSo;
import com.tobaconsulting.taowi_mobile.library.ParseXML;
import com.tobaconsulting.taowi_mobile.library.Preferences;
import com.tobaconsulting.taowi_mobile.model.MPoSoLineHdr;
import com.tobaconsulting.taowi_mobile.query.QPoSoLineHdr;
import com.tobaconsulting.taowi_mobile.view.DividerItemDecoration;

import org.evenos.utils.TestUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PurchaseOrderlineHdrActivity extends AppCompatActivity {

    public static final String TAG = PurchaseOrderlineHdrActivity.class.getSimpleName();
    public static final MediaType MEDIA_TYPE = MediaType.parse("text/x-markdown; charset=utf-8");
    public List<MPoSoLineHdr> mPoSoLineHdrList = new ArrayList<>();

    @Bind(R.id.recycler_po_so_line_hdr)
    public RecyclerView recyclerView;

    @Bind(R.id.pb_po_so_line_hdr)
    public ProgressBar progressBar;

    @Bind(R.id.textWarning)
    public TextView txtWarning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_po_so_line_hdr);

        ButterKnife.bind(this);

        OkHttpClient client = new OkHttpClient();

        String ipaddress = Preferences.getIpAddress(getBaseContext());
        String port = Preferences.getPort(getBaseContext());
        String url = TestUtils.getEndpoint(ipaddress, port);
        String username = BundlePass.getIntentExtra(getIntent().getExtras(), "username");
        String password = BundlePass.getIntentExtra(getIntent().getExtras(), "password");
        String C_Order_ID = BundlePass.getIntentExtra(getIntent().getExtras(), "C_Order_ID");

        String query = QPoSoLineHdr.QueryData("po_so_line", username, password, C_Order_ID);

        RequestBody requestBody = RequestBody.create(MEDIA_TYPE, query);

        Request request = new Request.Builder()
                .url(url)
                .addHeader("Content-Type", "application/xml")
                .post(requestBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        String data = response.body().string();
                        Log.i(TAG, "Response: " + data);
                        Document document = ParseXML.convertStringToDocument(data);
                        document.getDocumentElement().normalize();
                        NodeList nodeList = document.getElementsByTagName("DataRow");
                        mPoSoLineHdrList = GeneratePoSo.generatePoSoLineHdr(nodeList);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (mPoSoLineHdrList.size() == 0) {
                                    progressBar.setVisibility(View.INVISIBLE);
                                    txtWarning.setVisibility(View.VISIBLE);
                                } else {
                                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
                                    recyclerView.setLayoutManager(layoutManager);
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    recyclerView.addItemDecoration(new DividerItemDecoration(getBaseContext(), LinearLayout.VERTICAL));
                                    recyclerView.setAdapter(new APoSoHdr(mPoSoLineHdrList, new APoSoHdr.onItemClickListener() {
                                        @Override
                                        public void onItemClick(MPoSoLineHdr mPoSoLineHdr) {
                                            Intent intent = new Intent(PurchaseOrderlineHdrActivity.this, PurchaseOrderlineDtlActivity.class);
                                            intent.putExtra("username", BundlePass.getIntentExtra(getIntent().getExtras(), "username"));
                                            intent.putExtra("password", BundlePass.getIntentExtra(getIntent().getExtras(), "password"));
                                            intent.putExtra("C_Orderline_ID", mPoSoLineHdr.getC_Orderline_ID());
                                            startActivity(intent);
                                        }
                                    }));
                                    progressBar.setVisibility(View.INVISIBLE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    response.close();
                }
            }
        });


    }
}
