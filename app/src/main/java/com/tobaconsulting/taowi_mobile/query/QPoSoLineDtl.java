package com.tobaconsulting.taowi_mobile.query;

import org.evenos.utils.TestUtils;
import org.evenos.webservice.enums.WebServiceOperationName;
import org.evenos.webservice.fields.DataRow;
import org.evenos.webservice.nodes.ADLoginRequest;
import org.evenos.webservice.nodes.ModelCRUD;
import org.evenos.webservice.util.WebServiceOperation;

/**
 * Created by alvinoktavianus on 8/30/2016.
 */
public class QPoSoLineDtl {

    public static String GeneratePoSoLineDtl(String service_type, String username, String password, String C_OrderLine_ID) {
        ADLoginRequest loginRequest = TestUtils.getADLoginRequest(username, password);
        DataRow dataRow = new DataRow();
        dataRow.addDataField("C_OrderLine_ID", C_OrderLine_ID);
        ModelCRUD modelCRUD = new ModelCRUD(service_type);
        modelCRUD.setDataRow(dataRow);
        WebServiceOperation queryData = new WebServiceOperation(WebServiceOperationName.queryData, modelCRUD, loginRequest);
        return String.valueOf(queryData);
    }

}
