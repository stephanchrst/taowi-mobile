package com.tobaconsulting.taowi_mobile.query;

import org.evenos.utils.TestUtils;
import org.evenos.webservice.enums.WebServiceOperationName;
import org.evenos.webservice.fields.DataRow;
import org.evenos.webservice.nodes.ADLoginRequest;
import org.evenos.webservice.nodes.ModelCRUD;
import org.evenos.webservice.util.WebServiceOperation;

/**
 * Created by alvinoktavianus on 8/26/2016.
 */
public class QPoSoHdr {

    public static String QueryPO(String service_type, String username, String password) {
        ADLoginRequest loginRequest = TestUtils.getADLoginRequest(username, password);
        DataRow dataRow = new DataRow();
        dataRow.addDataField("IsSOTrx", "N");
        ModelCRUD modelCRUD = new ModelCRUD(service_type);
        modelCRUD.setDataRow(dataRow);
        WebServiceOperation queryData = new WebServiceOperation(WebServiceOperationName.queryData, modelCRUD, loginRequest);
        return String.valueOf(queryData);
    }

    public static String QuerySO(String service_type, String username, String password) {
        ADLoginRequest loginRequest = TestUtils.getADLoginRequest(username, password);
        DataRow dataRow = new DataRow();
        dataRow.addDataField("IsSOTrx", "Y");
        ModelCRUD modelCRUD = new ModelCRUD(service_type);
        modelCRUD.setDataRow(dataRow);
        WebServiceOperation queryData = new WebServiceOperation(WebServiceOperationName.queryData, modelCRUD, loginRequest);
        return String.valueOf(queryData);
    }

}
