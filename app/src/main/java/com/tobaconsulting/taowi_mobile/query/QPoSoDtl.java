package com.tobaconsulting.taowi_mobile.query;

import org.evenos.utils.TestUtils;
import org.evenos.webservice.enums.WebServiceOperationName;
import org.evenos.webservice.fields.DataRow;
import org.evenos.webservice.nodes.ADLoginRequest;
import org.evenos.webservice.nodes.ModelCRUD;
import org.evenos.webservice.util.WebServiceOperation;

/**
 * Created by alvinoktavianus on 8/26/2016.
 */
public class QPoSoDtl {

    public static String QueryPoDtl(String service_type, String username, String password, String C_Order_ID) {
        DataRow dataRow = new DataRow();
        dataRow.addDataField("IsSOTrx", "N");
        dataRow.addDataField("C_Order_ID", C_Order_ID);
        ADLoginRequest loginRequest = TestUtils.getADLoginRequest(username, password);
        ModelCRUD modelCRUD = new ModelCRUD(service_type);
        modelCRUD.setDataRow(dataRow);
        WebServiceOperation queryData = new WebServiceOperation(WebServiceOperationName.queryData, modelCRUD, loginRequest);
        return String.valueOf(queryData);
    }

    public static String QuerySoDtl(String service_type, String username, String password, String C_Order_ID) {
        DataRow dataRow = new DataRow();
        dataRow.addDataField("IsSOTrx", "Y");
        dataRow.addDataField("C_Order_ID", C_Order_ID);
        ADLoginRequest loginRequest = TestUtils.getADLoginRequest(username, password);
        ModelCRUD modelCRUD = new ModelCRUD(service_type);
        modelCRUD.setDataRow(dataRow);
        WebServiceOperation queryData = new WebServiceOperation(WebServiceOperationName.queryData, modelCRUD, loginRequest);
        return String.valueOf(queryData);
    }


}
